# README #

[Image gallery search](https://agileengine.gitlab.io/interview/test-tasks/beQwwuNFStubgcbH/)

[Application endpoints Postman collection](https://www.getpostman.com/collections/64dbde1625120eac957f)

### Properties ###

* spring.gallery.url - gallery service URL;
* spring.gallery.apiKey - gallery API key;
* spring.cache.ttl.milliseconds - cache TTL in milliseconds;
* spring.datasource.url - PostgreSQL database URL;
* spring.datasource.username - datasource username;
* spring.datasource.password - datasource password.

### Requirements ###

* Create PostgreSQL database;
* set properties;
* run Spring Boot application.