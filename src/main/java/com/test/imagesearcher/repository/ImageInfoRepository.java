package com.test.imagesearcher.repository;

import com.test.imagesearcher.data.ImageInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Image info repository.
 *
 * @author Vladimir
 */
public interface ImageInfoRepository extends JpaRepository<ImageInfo, String> {

    @Query(value = "SELECT * FROM image_info io WHERE CONCAT_WS(', ', io.author, io.camera, io.tags) ilike %:searchTerm%", nativeQuery = true)
    List<ImageInfo> search(@Param("searchTerm") String searchTerm);

}
