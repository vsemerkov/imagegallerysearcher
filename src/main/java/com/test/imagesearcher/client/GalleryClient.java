package com.test.imagesearcher.client;

import com.test.imagesearcher.config.GalleryConfig;
import com.test.imagesearcher.data.dto.GetTokenRequestDto;
import com.test.imagesearcher.data.dto.ImageFullInfoRestDto;
import com.test.imagesearcher.data.dto.ImageInfoListRestDto;
import com.test.imagesearcher.data.dto.TokenRestDto;
import com.test.imagesearcher.rest.exception.GalleryOperationException;
import com.test.imagesearcher.rest.interceptor.BearerAuthorizationInterceptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

/**
 * Gallery client.
 *
 * @author Vladimir
 */
@Component
public class GalleryClient {

    private static final Logger log = LogManager.getLogger(GalleryClient.class);

    @Autowired
    private GalleryConfig galleryConfig;

    private RestTemplate tokenRestTemplate = new RestTemplate();

    private RestTemplate operationsRestTemplate = new RestTemplate();

    @PostConstruct
    public void init() {
        // Problem. Token TTL isn't described in the task
        operationsRestTemplate.getInterceptors().add(new BearerAuthorizationInterceptor(this::getToken));
    }

    public ImageInfoListRestDto getImages(int page, int limit) {
        try {
            ResponseEntity<ImageInfoListRestDto> responseEntity = operationsRestTemplate.getForEntity(galleryConfig.getUrl() + "/images?page={page}&limit={limit}", ImageInfoListRestDto.class, page, limit);
            return responseEntity.getBody();
        } catch (Exception e) {
            throw processAndGetException("Error during get images operation", e);
        }
    }

    public ImageFullInfoRestDto getImage(String id) {
        try {
            ResponseEntity<ImageFullInfoRestDto> responseEntity = operationsRestTemplate.getForEntity(galleryConfig.getUrl() + "/images/{id}", ImageFullInfoRestDto.class, id);
            return responseEntity.getBody();
        } catch (Exception e) {
            throw processAndGetException("Error during get image by id operation. Id: " + id + ".", e);
        }
    }

    private String getToken() {
        try {
            ResponseEntity<TokenRestDto> responseEntity = tokenRestTemplate.postForEntity(galleryConfig.getUrl() + "/auth", new GetTokenRequestDto(galleryConfig.getApiKey()), TokenRestDto.class);
            return responseEntity.getBody().getToken();
        } catch (Exception e) {
            throw processAndGetException("Error during getting token", e);
        }
    }

    /**
     * Processes exception to {@link GalleryOperationException}.
     *
     * @param errorStr error string
     * @param e        exception
     * @return gallery operation exception
     */
    private GalleryOperationException processAndGetException(String errorStr, Exception e) {
        int rawStatusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        if (e instanceof RestClientResponseException) {
            RestClientResponseException restClientResponseException = (RestClientResponseException) e;
            errorStr += " " + ((RestClientResponseException) e).getResponseBodyAsString();
            rawStatusCode = restClientResponseException.getRawStatusCode();
        }

        log.error(errorStr, e);
        return new GalleryOperationException(errorStr, rawStatusCode);
    }
}
