package com.test.imagesearcher.service;

import com.test.imagesearcher.data.ImageInfo;

import java.util.List;

/**
 * Image service interface.
 *
 * @author Vladimir
 */
public interface ImageService {

    /**
     * Finds image information by the search term.
     * Fields for search: author, camera, tags
     *
     * @param searchTerm search term
     * @return image information
     */
    List<ImageInfo> search(String searchTerm);
}
