package com.test.imagesearcher.service;

import com.test.imagesearcher.client.GalleryClient;
import com.test.imagesearcher.data.ImageInfo;
import com.test.imagesearcher.data.dto.ImageFullInfoRestDto;
import com.test.imagesearcher.data.dto.ImageInfoListRestDto;
import com.test.imagesearcher.data.dto.ImageInfoRestDto;
import com.test.imagesearcher.repository.ImageInfoRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Image service interface implementation.
 *
 * @author Vladimir
 */
@Service
public class ImageServiceImpl implements ImageService {

    private static final Logger log = LogManager.getLogger(ImageServiceImpl.class);
    private static final int MAX_THREAD_COUNT = 2;
    private static final int MAX_PAGE_SIZE = 20;

    @Autowired
    private GalleryClient galleryClient;

    @Autowired
    private ImageInfoRepository imageInfoRepository;

    @Scheduled(fixedDelayString = "${spring.cache.ttl.milliseconds}")
    @Transactional
    public void refreshCache() {
        imageInfoRepository.deleteAll();

        ImageInfoListRestDto imageInfoListRestDto = galleryClient.getImages(1, 1);
        int itemsCount = imageInfoListRestDto.getPageCount();
        int threadCount = Math.min(itemsCount, MAX_THREAD_COUNT);
        int itemsToThread = itemsCount / threadCount;
        int pageSize = Math.min(itemsToThread, MAX_PAGE_SIZE);
        int pagesToThread = itemsToThread / pageSize;

        int pageNumber = 1;
        List<Callable<Boolean>> callables = new ArrayList<>(threadCount);
        for (int i = 1; i <= threadCount; i++) {
            int startPage = pageNumber;
            pageNumber += pagesToThread;
            int endPage = i == threadCount ? pageNumber + 1 : pageNumber;
            callables.add(() -> loadPageItems(startPage, endPage, pageSize));
        }

        try {
            ExecutorService executor = Executors.newFixedThreadPool(threadCount);
            executor.invokeAll(callables);
            executor.shutdown();
        } catch (Exception e) {
            log.error("Cannot load images for cache", e);
        }
    }

    private boolean loadPageItems(int startPage, int endPage, int pageSize) {
        boolean load = true;
        int page = startPage;
        while (page < endPage && load) {
            ImageInfoListRestDto imageInfoListRestDto = galleryClient.getImages(page, pageSize);

            List<ImageInfoRestDto> imageRestDtos = imageInfoListRestDto.getPictures();
            if (CollectionUtils.isNotEmpty(imageRestDtos)) {
                imageRestDtos.forEach(imageInfoRestDto -> {
                    ImageFullInfoRestDto imageFullInfoRestDto = galleryClient.getImage(imageInfoRestDto.getId());
                    ImageInfo imageInfo = convertToImageInfo(imageFullInfoRestDto);
                    imageInfoRepository.save(imageInfo);
                });
            }

            load = imageInfoListRestDto.getHasMore();
            page++;
        }
        return true;
    }

    private ImageInfo convertToImageInfo(ImageFullInfoRestDto source) {
        ImageInfo target = new ImageInfo();
        target.setId(source.getId());
        target.setAuthor(source.getAuthor());
        target.setCamera(source.getCamera());
        target.setTags(source.getTags());
        target.setCroppedPicture(source.getCroppedPicture());
        target.setFullPicture(source.getFullPicture());
        return target;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ImageInfo> search(String searchTerm) {
        return imageInfoRepository.search(searchTerm);
    }
}
