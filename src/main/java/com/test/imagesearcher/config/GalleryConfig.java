package com.test.imagesearcher.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Gallery configuration properties.
 *
 * @author Vladimir
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "spring.gallery")
public class GalleryConfig {

    /**
     * Gallery url
     */
    private String url;

    /**
     * Gallery API key for authorization token getting
     */
    private String apiKey;
}
