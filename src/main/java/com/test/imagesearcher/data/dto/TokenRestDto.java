package com.test.imagesearcher.data.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Token response DTO.
 *
 * @author Vladimir
 */
@Getter
@Setter
public class TokenRestDto {

    /**
     * Authentication sign
     */
    private boolean auth;

    /**
     * Token
     */
    private String token;
}
