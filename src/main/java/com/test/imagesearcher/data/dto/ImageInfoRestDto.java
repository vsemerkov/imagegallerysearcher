package com.test.imagesearcher.data.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Response DTO from gallery with short information about image.
 *
 * @author Vladimir
 */
@Getter
@Setter
public class ImageInfoRestDto {

    private String id;

    @JsonProperty("cropped_picture")
    private String croppedPicture;
}
