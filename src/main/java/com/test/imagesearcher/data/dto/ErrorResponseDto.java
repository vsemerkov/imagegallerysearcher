package com.test.imagesearcher.data.dto;

import com.test.imagesearcher.rest.ExceptionController;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Response DTO with error message.
 * Used in {@link ExceptionController}.
 *
 * @author Vladimir
 */
@Getter
@Setter
@AllArgsConstructor
public class ErrorResponseDto {

    /**
     * Error message
     */
    private String message;
}
