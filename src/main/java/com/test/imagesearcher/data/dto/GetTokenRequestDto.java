package com.test.imagesearcher.data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Request DTO for getting token.
 *
 * @author Vladimir
 */
@Getter
@Setter
@AllArgsConstructor
public class GetTokenRequestDto {

    /**
     * Gallery API key
     */
    private String apiKey;
}
