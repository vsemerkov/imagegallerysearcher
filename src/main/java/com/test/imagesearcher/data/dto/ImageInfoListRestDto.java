package com.test.imagesearcher.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Response DTO from gallery with full information about images.
 *
 * @author Vladimir
 */
@Getter
@Setter
public class ImageInfoListRestDto {

    private List<ImageInfoRestDto> pictures;

    private Integer page;

    private Integer pageCount;

    public Boolean hasMore;
}
