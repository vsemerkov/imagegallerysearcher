package com.test.imagesearcher.data.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Response DTO from gallery with full information about image.
 *
 * @author Vladimir
 */
@Getter
@Setter
public class ImageFullInfoRestDto extends ImageInfoRestDto {

    private String author;

    private String camera;

    private String tags;

    @JsonProperty("full_picture")
    private String fullPicture;
}
