package com.test.imagesearcher.data;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Image information.
 * Used in cache.
 *
 * @author Vladimir
 */
@Getter
@Setter
@Entity
@Table(name = "image_info")
public class ImageInfo {

    @Id
    private String id;

    @Column
    private String author;

    @Column
    private String camera;

    @Column
    private String tags;

    @Column(name = "cropped_picture")
    private String croppedPicture;

    @Column(name = "full_picture")
    private String fullPicture;
}
