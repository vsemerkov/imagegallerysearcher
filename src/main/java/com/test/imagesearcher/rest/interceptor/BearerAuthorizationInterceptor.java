package com.test.imagesearcher.rest.interceptor;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.function.Supplier;

/**
 * Interceptor for putting authorization token to requests.
 *
 * @author Vladimir
 */
public class BearerAuthorizationInterceptor implements ClientHttpRequestInterceptor {

    private Supplier<String> tokenSupplier;

    public BearerAuthorizationInterceptor(Supplier<String> tokenSupplier) {
        this.tokenSupplier = tokenSupplier;
    }


    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        request.getHeaders().setBearerAuth(tokenSupplier.get());
        return execution.execute(request, body);
    }
}
