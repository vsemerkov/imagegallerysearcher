package com.test.imagesearcher.rest;

import com.test.imagesearcher.client.GalleryClient;
import com.test.imagesearcher.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Image controller.
 *
 * @author Vladimir
 */
@RestController
@RequestMapping
public class ImageController {

    private static Integer PAGE_LIMIT = 5;

    @Autowired
    private GalleryClient galleryClient;

    @Autowired
    private ImageService imageService;

    @GetMapping("/images")
    public ResponseEntity getImages(@RequestParam(value = "page", defaultValue = "1") int page) {
        return ResponseEntity.ok(galleryClient.getImages(page, PAGE_LIMIT));
    }

    @GetMapping("/images/{id}")
    public ResponseEntity getImages(@PathVariable String id) {
        return ResponseEntity.ok(galleryClient.getImage(id));
    }

    @GetMapping("/search/{searchTerm}")
    public ResponseEntity search(@PathVariable String searchTerm) {
        // Pagination is needed, but not described in the task
        return ResponseEntity.ok(imageService.search(searchTerm));
    }
}
