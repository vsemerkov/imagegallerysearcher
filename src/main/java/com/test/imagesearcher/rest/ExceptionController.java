package com.test.imagesearcher.rest;

import com.test.imagesearcher.data.dto.ErrorResponseDto;
import com.test.imagesearcher.rest.exception.GalleryOperationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * Exception handler.
 *
 * @author Vladimir
 */
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(GalleryOperationException.class)
    @ResponseBody
    public ResponseEntity businessLogicException(GalleryOperationException e, HttpServletResponse response) {
        ErrorResponseDto errorResponseDto = new ErrorResponseDto(e.getMessage());
        return ResponseEntity.status(e.getRawStatusCode()).body(errorResponseDto);
    }
}
