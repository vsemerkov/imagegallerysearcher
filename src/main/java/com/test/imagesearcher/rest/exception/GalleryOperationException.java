package com.test.imagesearcher.rest.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Utility exception for correct gallery exception handling.
 *
 * @author Vladimir
 */
@Getter
@Setter
public class GalleryOperationException extends RuntimeException {

    /**
     * Raw HTTP status code
     */
    private int rawStatusCode;

    public GalleryOperationException(String message, int rawStatusCode) {
        super(message);
        this.rawStatusCode = rawStatusCode;
    }
}
